<?php

namespace Lar\EntityCarrier;

use Illuminate\Support\Facades\Facade as FacadeIlluminate;

/**
 * Class Facade.
 *
 * @package Lar
 */
class Facade extends FacadeIlluminate
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return EntityCarrier::class;
    }
}
