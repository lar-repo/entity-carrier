<?php

namespace Lar\EntityCarrier\Core\Wrappers;

class ReturnWrapper extends Wrapper
{
    /**
     * @param string $data
     * @return string
     */
    protected function wrap(string $data): string
    {
        return "return {$data};";
    }
}
