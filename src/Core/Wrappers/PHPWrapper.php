<?php

namespace Lar\EntityCarrier\Core\Wrappers;

class PHPWrapper extends Wrapper
{
    /**
     * @param string $data
     * @return string
     */
    protected function wrap(string $data): string
    {
        return "<?php\n\n{$data}";
    }
}
