<?php

namespace Lar\EntityCarrier\Core\Savers;

/**
 * Class File.
 *
 * @package Lar\EntityCarrier\Core\Savers
 */
class File extends Driver
{
    /**
     * Save method.
     * @return int
     */
    public function save(): int
    {
        return file_put_contents($this->file, $this->getData());
    }
}
