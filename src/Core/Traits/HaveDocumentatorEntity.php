<?php

namespace Lar\EntityCarrier\Core\Traits;

use Lar\EntityCarrier\Core\Entities\DocumentorEntity;

/**
 * Trait HaveDocumentatorEntity.
 *
 * @package Lar\EntityCarrier\Core\Traits
 */
trait HaveDocumentatorEntity
{
    /**
     * @var null|DocumentorEntity
     */
    protected $doc = null;

    /**
     * Documentor class access.
     *
     * @param \Closure|DocumentorEntity $call
     * @return $this
     */
    public function doc($call)
    {
        if ($call instanceof DocumentorEntity) {
            $this->doc = $call;
        } elseif (is_embedded_call($call)) {
            if (! $this->doc) {
                $this->doc = new DocumentorEntity();
            }

            call_user_func($call, $this->doc);
        }

        return $this;
    }
}
